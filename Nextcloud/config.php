<?php
$CONFIG = array (
  'trusted_domains' =>
  array (
    0 => 'localhost',
    1 => '*'
  ),
  'dbname' => 'nextcloud',
  'dbhost' => 'mariadb',
  'dbport' => '3306',
  'mysql.utf8mb4' => true,
  'dbuser' => 'nextclouduser',
  'dbpassword' => 'nextcloud',
  'installed' => true,
);
